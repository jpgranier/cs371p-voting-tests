DEFAULT_GOAL := all
MAKEFLAGS += --no-builtin-rules

CHECKTESTDATA := checktestdata

INFILES := $(shell ls *.in)


FILE=output.txt
VARIABLE=`cat $(FILE)`

hello:
	echo $(VARIABLE)

check-all:
	for file in $(INFILES) ; do $(CHECKTESTDATA) RunVoting.ctd $$file >> output.txt 2>&1; done

docker:
	docker run -it -v $(PWD):/usr/python -w /usr/python gpdowning/python

